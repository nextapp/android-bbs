package cn.nextapp.app.bbs;

import cn.nextapp.app.bbs.api.ApiException;
import cn.nextapp.app.bbs.api.NextAppClient;
import cn.nextapp.app.bbs.api.NextAppController;
import cn.nextapp.app.bbs.common.UIHelper;
import cn.nextapp.app.bbs.entity.URLs;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Toast;

/**
 * 启动界面
 * @author Winter Lau
 */
public class Start extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		final View view = View.inflate(this, R.layout.start, null);
		setContentView(view);		
		
		//渐变展示启动屏
		AlphaAnimation aa = new AlphaAnimation(0.3f,1.0f);
		aa.setDuration(2000);
		view.startAnimation(aa);
		aa.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationEnd(Animation arg0) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationStart(Animation animation) {}
			
		});

		Handler mHandler = new Handler(){
			public void handleMessage(Message msg) {
				if(msg.what == 1)
					UIHelper.showHome(Start.this);				
				else if(msg.what == 0){
					Toast.makeText(Start.this, R.string.network_not_connected, Toast.LENGTH_LONG).show();
				}
				else if(msg.obj != null){
					ApiException ae = (ApiException)msg.obj;
					ae.makeToast(Start.this);
				}
			}
		};
		NextAppController.getUrls(this, mHandler);
		
	}
	
}
