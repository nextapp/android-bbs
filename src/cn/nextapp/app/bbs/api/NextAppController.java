package cn.nextapp.app.bbs.api;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import cn.nextapp.app.bbs.NextAppContext;
import cn.nextapp.app.bbs.entity.URLs;

/**
 * NextApp 客户端业务线程控制器
 * @author liux
 */
public class NextAppController {
	public static void getUrls(final Activity activity,final Handler handler)
	{
		new Thread(){
        	public void run() {
				Message msg = new Message();
				boolean networkOk = NextAppClient.isNetworkConnected(activity.getApplicationContext());
				try{ 
					//加载初始化数据
					if(networkOk == true){						
						URLs urls = ((NextAppContext)activity.getApplication()).getUrls();
						msg.what = 1;
					}
					else
						msg.what = 0;
				}catch (ApiException e){
					msg.what = -1;
					msg.obj = e;
			    }
				handler.sendMessage(msg);
			}
        }.start();
	}
}
