package cn.nextapp.app.bbs;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;
import cn.nextapp.app.bbs.common.UIHelper;

public class Main extends TabActivity implements OnCheckedChangeListener{

	private TabHost mHost;
	private RadioGroup radioderGroup;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //实例化TabHost
        mHost=this.getTabHost();
        
        //添加选项卡
        mHost.addTab(mHost.newTabSpec("post").setIndicator("post")
    			.setContent(new Intent(this,PostTabs.class)));
        mHost.addTab(mHost.newTabSpec("catalog").setIndicator("catalog")
        		.setContent(new Intent(this,PostListHot.class)));
        mHost.addTab(mHost.newTabSpec("message").setIndicator("message")
        		.setContent(new Intent(this,PostListHot.class)));
        mHost.addTab(mHost.newTabSpec("setting").setIndicator("setting")
        		.setContent(new Intent(this,PostListHot.class)));
        
        radioderGroup = (RadioGroup) findViewById(R.id.maintab_radiogroup);
		radioderGroup.setOnCheckedChangeListener(this);
		
    }
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch(checkedId){
		case R.id.maintab_radio_btn_post:
			mHost.setCurrentTabByTag("post");
			break;
		case R.id.maintab_radio_btn_catalog:
			mHost.setCurrentTabByTag("catalog");
			break;
		case R.id.maintab_radio_btn_message:
			mHost.setCurrentTabByTag("message");
			break;
		case R.id.maintab_radio_btn_setting:
			mHost.setCurrentTabByTag("setting");
			break;
		}		
	}
	/**
	 * 创建menu
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	/**
	 * 菜单被显示之前的事件
	 */
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}

	/**
	 * 处理menu的事件
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		UIHelper.menuItemSelectedAction(this,item);
		return true;
	}
}